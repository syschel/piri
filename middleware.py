# -*- coding: utf-8 -*-
from cms.models import Sites

class RegionsMiddleware(object):
    """
    Загружаем сразу сайты, в request
    """

    def process_request(self, request):
        # Главный домен
        request.site_main = Sites.objects.get(root=True)
        # Текущий домен
        site = Sites.objects.filter(domain=request.META.get("HTTP_HOST"))
        if site:
            request.site_current = site[0]
        else:
            request.site_current = request.site_main

