#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User, UserManager #, BaseUserManager, AbstractBaseUser #UserManager

def md5(str_hash = "hash text"):
    import hashlib
    hsh = hashlib.md5()
    hsh.update(str_hash)
    hash = hsh.hexdigest()
    return hash



class Person(User):
    """ Профиль пользователя """

    MALE, FEMALE = range(1,3)
    SEX = (
        (MALE, _(u'#Мужской')),
        (FEMALE, _(u'#Женский'))
    )
    phone               = models.CharField(_(u'#Телефон'), max_length=30, blank=True, null=True)
    birthday            = models.DateField(_(u'#Дата рождения'), help_text=_(u"формат даты  дд.мм.гггг"), blank=True, null=True)
    skype               = models.CharField(_(u'#skype'), max_length=50, blank=True, null=True)
    icq                 = models.CharField(_(u'#icq'), max_length=20, blank=True, null=True)
    sex                 = models.IntegerField(_(u'#Пол пользователя'), max_length=1, default=0, choices=SEX)


    objects = UserManager()

    class Meta:
        verbose_name = _(u'#Пользователи')
        verbose_name_plural = _(u'#Пользователи')
        # unique_together = ('email',)

class NetworkAuth(models.Model):
    #типы соцсетей, в которых разрешена регистрация
    name = models.CharField(_(u'#Наше название соцсети'), blank=True, null=True, max_length=50)
    codename = models.CharField(_(u'#кодовое название соцсети'), max_length=100)
    url = models.CharField(_(u'#ссылка на ресурс'), blank=True, null=True, max_length=100)
    description = models.CharField(_(u'#Описание'), blank=True, null=True, max_length=250)

    def __unicode__(self):
        return u"Соцсеть: %s" % self.name

    class Meta:
        verbose_name = u'Соцсеть'
        verbose_name_plural = u'Список соцсетей '


class PersonSocial(models.Model):
    person = models.ForeignKey(Person, verbose_name=_(u'#Пользователь'))
    auth = models.ForeignKey(NetworkAuth, verbose_name=_(u'#Соцсеть'))
    link_to_profile = models.CharField(_(u'#Ссылка на профиль'), blank=True, null=True, max_length=100)
    uid = models.CharField(_(u'#id человека в этой социалке'), max_length=30)

    def __unicode__(self):
        return u"Account в сети: %s - %s" % (self.person, self.auth)

    class Meta:
        verbose_name = _(u'#Account в соцсети')
        verbose_name_plural = _(u'#Account в соцсетях')