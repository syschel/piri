# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('person.views',
    url(r'^$', 'profile', name='edit_profile'),
    url(r'^login/$', 'user_login', name="login"),
    url(r'^logout/$', 'user_logout', name='logout'),
    url(r'^registr/$', 'user_registr', name='registr'),
    url(r'^ulogin/$', 'ulogin', name='ulogin'),


)