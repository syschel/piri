# -*- coding: utf8 -*-
import re
from django.db.models.query_utils import Q

# from django.contrib.auth import authenticate

from person.models import Person

class PersonalModelBackend(object):

    supports_anonymous_user = False
    supports_object_permissions = False

    def authenticate(self, logins=None, passwords=None):
        try:
            phone       = re.sub(u"[^0-9+]*", "", logins)
            phone       = re.sub(u"^8", "+7", phone)
            if phone:
                person = Person.objects.filter(phone=phone)
            else:
                person = Person.objects.filter(email=logins)
            if person:
                person = person[0]
                if person.check_password(passwords):
                    person.backend = 'person.auth_backends.PersonalModelBackend'
                    return person
                else:
                    return None
            else:
                return None


        except Person.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Person.objects.get(pk=user_id)
        except Person.DoesNotExist:
            return None

