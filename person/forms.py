# -*- coding: utf-8 -*-
import re
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db.models import Q

from person.models import User, Person
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    username = forms.RegexField(label=_("Username"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text = _("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages = {
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text = _("Enter the same password as above, for verification."))

    class Meta:
        model = Person
        fields = ("username",)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            Person.objects.get(username=username)
        except Person.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def clean_email(self):
        data = self.cleaned_data['email']
        if data:
            if Person.objects.filter(email=data).exists():
                raise forms.ValidationError(u"#Этот e-mail уже занят")
        return data

    def clean_phone(self):
        data = self.cleaned_data['phone']
        data = re.sub(u"[^0-9+]*", "", data)
        data = re.sub(u"^8", "+7", data)
        if data:
            if Person.objects.filter(Q(phone=data), ~Q(username=self.cleaned_data['username'])).exists():
                raise forms.ValidationError(_(u"#Этот телефон уже занят"))
        return data

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    username = forms.RegexField(
        label=_("Username"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text = _("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages = {
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password = ReadOnlyPasswordHashField(label=_("Password"),
        help_text=_("Raw passwords are not stored, so there is no way to see "
                    "this user's password, but you can change the password "
                    "using <a href=\"password/\">this form</a>."))

    def clean_password(self):
        return self.initial["password"]

    def clean_email(self):
        data = self.cleaned_data['email']
        if data:
            if Person.objects.filter(Q(email=data), ~Q(username=self.cleaned_data['username'])).exists():
                raise forms.ValidationError(_("This email already used"))
        return data

    def clean_phone(self):
        data = self.cleaned_data['phone']
        data = re.sub(u"[^0-9\+]*", "", data)
        data = re.sub(u"^8", "+7", data)
        if data:
            if Person.objects.filter(Q(phone=data), ~Q(username=self.cleaned_data['username'])).exists():
                raise forms.ValidationError(_("This phone already used"))
        return data

    class Meta:
        model = Person

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

class AdminPasswordChangeForm(forms.Form):
    """
    A form used to change the password of a user in the admin interface.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password (again)"),
                                widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(AdminPasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        """
        Saves the new password.
        """
        self.user.set_password(self.cleaned_data["password1"])
        if commit:
            self.user.save()
        return self.user


class RegistrationForms(forms.ModelForm):
    """ Форма регистрации """
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password (again)"), widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.usernames = False
        self.email = None
        self.phone = None
        super(RegistrationForms, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data

        if cleaned_data.get('email'):
            if Person.objects.filter(Q(email=cleaned_data.get('email')) | Q(username=cleaned_data['email'])).exists():
                raise forms.ValidationError(u"#Этот e-mail уже занят")
            self.email = cleaned_data.get('email')
            self.usernames = cleaned_data.get('email')

        if cleaned_data.get('phone'):
            phone = re.sub(u"[^0-9+]*", "", cleaned_data.get('phone'))
            phone = re.sub(u"^8", "+7", phone)
            if len(phone) < 5:
                raise forms.ValidationError(_(u'#Вы указали не верный формат телефона'))
            else:
                if Person.objects.filter(Q(phone=phone) | Q(username=phone)).exists():
                    raise forms.ValidationError(u"#Этот телефон уже занят")

                self.phone = phone
                if not self.usernames:
                    self.usernames = phone

        if not self.usernames:
            raise forms.ValidationError(_(u'#Обязательно заполнить емайл или телефон'))
        return cleaned_data

    def clean_password2(self):
        cleaned_data = self.cleaned_data
        if cleaned_data.get('password1') and cleaned_data.get('password2'):
            if cleaned_data.get('password1') != cleaned_data.get('password2'):
                raise forms.ValidationError(_(u"Пароли не совпали."))
        else:
            raise forms.ValidationError(_(u"Не выбран пароль"))

        return cleaned_data

    def save(self):
        user = Person.objects.create(
            phone=self.phone,
            email=self.email,
            username=self.usernames,
            is_active=True
        )
        user.set_password(self.cleaned_data["password1"])
        user.save()

    class Meta:
        model = Person
        fields = ('email', 'phone', 'password1', 'password2')

class ProfileForms(forms.ModelForm):
    """ Форма регистрации """

    birthday = forms.DateField(input_formats=('%d.%m.%Y',), widget=forms.DateInput(format = '%d.%m.%Y'),\
                               help_text=_(u"формат даты  дд.мм.гггг"), label=_(u'#Дата рождения'), required=False)

    def __init__(self, *args, **kwargs):
        super(ProfileForms, self).__init__(*args, **kwargs)
        self.a=kwargs.get('instance') #
        try:
            self.initial["birthday"]    = self.a.person.birthday
            self.initial["phone"]       = self.a.person.phone
            self.initial["skype"]       = self.a.person.skype
            self.initial["icq"]         = self.a.person.icq
            self.initial["sex"]         = self.a.person.sex
        except:
            pass

        self.fields['birthday'].widget.attrs['class'] = "datepicker"


    def clean_phone(self):
        cleaned_data = self.cleaned_data
        if 'phone' in cleaned_data and cleaned_data.get('phone'):
            phone = re.sub(u"[^0-9+]*", "", cleaned_data.get('phone'))
            phone = re.sub(u"^8", "+7", phone)
            if len(phone) < 5:
                raise forms.ValidationError(_(u'#Вы указали не верный формат телефона'))
            if Person.objects.filter(Q(phone=phone) | Q(username=phone)).filter(~Q(pk=self.a.pk)).exists():
                    raise forms.ValidationError(u"#Этот телефон уже занят")
            cleaned_data['phone'] = phone
            return cleaned_data['phone']
    def clean_email(self):
        if Person.objects.filter(Q(email=self.cleaned_data.get('email')) | Q(username=self.cleaned_data['email'])).filter(~Q(pk=self.a.pk)).exists():
            raise forms.ValidationError(u"#Этот e-mail уже занят")
        return self.cleaned_data['email']


    def save(self):
        # Ну не должно быть этой ериси тут, не должно. Но просто по form.save() ничего не сохраняло. Былиииин.
        user = Person.objects.get(pk=self.a.pk)
        user.first_name = self.cleaned_data.get("first_name")
        user.last_name = self.cleaned_data.get("last_name")
        user.email = self.cleaned_data.get("email")
        user.phone = self.cleaned_data.get("phone")
        user.birthday = self.cleaned_data.get("birthday")
        user.skype = self.cleaned_data.get("skype")
        user.icq = self.cleaned_data.get("icq")
        user.sex = self.cleaned_data.get("sex")
        user.save()

    class Meta:
        model = Person
        fields = ('first_name', 'last_name', 'email', 'phone', 'birthday', 'skype', 'icq', 'sex',)