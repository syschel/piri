#-*- coding:utf-8 -*-
import re
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
from person.auth_backends import PersonalModelBackend as auth_backends
from django.contrib.auth.models import AnonymousUser

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

from django.db.models.query_utils import Q

from person.models import Person, User, NetworkAuth, PersonSocial
from person.forms import RegistrationForms, ProfileForms

@render_to('person/login.html')
def user_login(request):
    """
    Форма авторизации
    """
    error = []
    if request.method == 'POST':
        post = request.POST
        logins      = post.get("login", False)
        password    = post.get("password", False)
        user        = auth_backends().authenticate(logins=logins, passwords=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse("edit_profile",))
        else:
            error = _(u"#Не удалось войти")

    return {'request':request, 'error':error}


def user_logout(request):
    """
    Выход
    """
    logout(request)
    return HttpResponseRedirect("/")

@render_to('person/registr.html')
def user_registr(request):
    """
    регистрация на сайте
    """
    error = []
    if request.method == 'POST':

        form = RegistrationForms(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("edit_profile",))

    else:
        form = RegistrationForms()
    return {'request': request, 'form': form}

@login_required(login_url='/')
@render_to('person/profile.html')
def profile(request):
    """
    Профиль пользователя
    """
    social = PersonSocial.objects.filter(person=request.user)
    if request.method == 'POST':
        form = ProfileForms(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("edit_profile"))
    else:
        form = ProfileForms(instance=request.user)

    return {'request': request, 'form': form, 'social': social}

def account_update(account, response):
    """
    Дозаполнение профиля через данные соцсети
    """
    if not account.last_name and 'last_name' in response:
        account.last_name    = response['last_name'] if response['last_name'] else None
    if not account.first_name and 'first_name' in response:
        account.first_name           = response['first_name'] if response['first_name'] else None
    if not account.birthday and response['bdate']:
        d, m, y = response['bdate'].split('.')
        account.birthday = datetime(int(y), int(m), int(d))
    account.save()
    return True


@csrf_exempt
def ulogin(request):
    """
    Авторизация через соцсети + подвязка к профилю
    """
    if request.method == 'POST' and 'token' in request.POST:
        import urllib
        import random
        import json
        url = 'http://ulogin.ru/token.php?token=%s&host=%s' % (request.POST['token'], request.META['HTTP_HOST'])
        response = json.loads(urllib.urlopen(url).read())
        """
        uid - уникальный id от соцсети
        network - соцсеть
        identity - ссылка на аккаунт

        first_name - имя пользователя,
        last_name - фамилия,
        email - e-mail,
        nickname - псевдоним,
        bdate - дата рождения,
        sex - пол, phone - телефон,
        photo - квадратная аватарка (до 100*100),
        photo_big - самая большая аватарка, которая выдаётся выбранной соц. сетью,
        city - город,
        country - страна.
        """
        if response and not 'error' in response:

            networks = NetworkAuth.objects.filter(codename=response['network'])

            if len(networks)>0:
                network = networks[0]
            else:
                #Если такой соцсети у нас ещё нет, то добавляем
                if 'profile' in response:
                    url =  '/'.join(response['profile'].split('/')[:3]) + '/' #Вычленяем адрес соцсети из profile

                network = NetworkAuth.objects.create(name = response['network'], codename = response['network'], url = url)
            uids = PersonSocial.objects.filter(uid = response['uid'], auth = network)
            #print uids
            uid = 0
            if uids:
                uid = uids[0]
                #print uid

            if request.user and request.user.is_authenticated():
                #Авторизован
                if uid:
                    #Проверить связку и либо дозаполняем либо перепривязываем
                    #print 'auth and uid'
                    if uid.person == request.user.person:
                        #дозаполняем
                        account_update(request.user.person, response)
                        #print 'auth and uid and  double'
                        pass
                    else:
                        #Перепривязываем
                        #print 'auth and uid and NOT double'
                        uid.person = request.user.person
                        uid.save()
                        pass

                else:
                    #Привязываем
                    #print 'auth, no uid'
                    uid = PersonSocial.objects.create(person = request.user.person, link_to_profile = response['profile'], auth = network, uid = response['uid'])
                    #Дозаполняем
                    account_update(request.user.person, response)
            else:
                #Не авторизован
                if uid:
                    #Авторизуем
                    user = User.objects.filter(Q(username=u"%s" % uid.person.email) | Q(email=u"%s" % uid.person.email))[0]
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    auth.login(request, user)
                else: # если такого ещё нет
                    #Проверяем связку с email и либо привязываем, либо создаем
                    puser = Person.objects.filter(Q(email=response['email']) | Q(username=response['email']))
                    if puser:
                        #Привязываем
                        uid = PersonSocial.objects.create(person = puser[0], link_to_profile = response['profile'], auth = network, uid = response['uid'])
                        #Дозаполняем
                        account_update(puser[0], response)
                        #авторизуем
                        user = User.objects.filter(Q(username=u"%s" % puser[0].email) | Q(email=u"%s" % puser[0].email))[0]
                        user.backend = 'person.auth_backends.PersonalModelBackend' #'django.contrib.auth.backends.ModelBackend'
                        auth.login(request, user)
                    else:
                        #Создаем person, создаем uid)
                        new_pass = ''
                        for i in range(6):
                            new_pass += random.choice('0123456789abcdefg')
                        person                  = Person.objects.create_user(response['email'], response['email'], new_pass)
                        person.last_name        = response['last_name'] if 'last_name' in response else None
                        person.first_name       = response['first_name'] if 'first_name' in response else None
                        person.is_stuff         = False
                        person.is_active        = True
                        if 'bdate' in response and response['bdate']:
                            d, m, y = response['bdate'].split('.')
                            person.birthday = datetime(int(y), int(m), int(d))
                        person.save()

                        #user = User.objects.filter(username=u"%s" % response['email'])
                        user = User.objects.filter(Q(username=u"%s" % response['email']) | Q(email=u"%s" % response['email']))[0]
                        user.backend = 'person.auth_backends.PersonalModelBackend' #'django.contrib.auth.backends.ModelBackend'
                        auth.login(request, user)

                        #print 'no auth, no uid, no person'
                        pass


        next = request.GET['next']
        if next == reverse('ulogin') or next == reverse('login') or next == reverse('registr'):
            next = reverse('edit_profile')
        return HttpResponseRedirect(next)
    else:
        #return HttpResponseRedirect(reverse('index_gigdate'))
        return {}