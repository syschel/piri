# -*- coding: utf-8 -*-

from django.contrib import admin
from tinymce.widgets import TinyMCE
from django.db import models
from django.contrib.sites.models import Site
from seo.models import SeoDefault

class SeoDefaultAdmin(admin.ModelAdmin):
    list_display = ('name', 'seo_def')
    list_display_links = ('name', 'seo_def')


admin.site.register(SeoDefault, SeoDefaultAdmin)


