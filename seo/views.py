#-*- coding:utf-8 -*-
import re

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

from django.db.models.query_utils import Q

from seo.models import SeoDefault
from seo.forms import SeoForms


@render_to('seo/seo_index.html')
def seo_index(request):
    """
    СЕОшное СЕО
    """
    return {'request':request}


@render_to('seo/tours_direct_url.html')
def tours_direct_url(request):
    """
    Конструктор ссылок для директа, под туризм
    """
    return {'request':request}