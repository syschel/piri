# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('seo.views',
    url(r'^$', 'seo_index', name='seo_index'),
    url(r'^tours_direct_url/$', 'tours_direct_url', name='seo_tours_direct_url'),

)