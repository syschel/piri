#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse


class SeoDefault(models.Model):
    """ Дефлтовские настройки СЕО """
    TYPE_SEO = (
        ('title_default', _(u'Title Default')),
        ('h1_default', _(u'H1 Default')),
        ('keywords_default', _(u'Keywords Default')),
        ('description_default', _(u'Description Default')),
        ('seotext_default', _(u'Seo_Text Default')),

        ('title_cms_section', _(u'Title cms_section')),
        ('h1_cms_section', _(u'H1 cms_section')),
        ('keywords_cms_section', _(u'Keywords cms_section')),
        ('description_cms_section', _(u'Description cms_section')),
        ('seotext_cms_section', _(u'Seo_Text cms_section')),

        ('title_cms_subsection', _(u'Title cms_subsection')),
        ('h1_cms_subsection', _(u'H1 cms_subsection')),
        ('keywords_cms_subsection', _(u'Keywords cms_subsection')),
        ('description_cms_subsection', _(u'Description cms_subsection')),
        ('seotext_cms_subsection', _(u'Seo_Text cms_subsection')),

        ('title_cms_pages', _(u'Title cms_pages')),
        ('h1_cms_pages', _(u'H1 cms_pages')),
        ('keywords_cms_pages', _(u'Keywords cms_pages')),
        ('description_cms_pages', _(u'Description cms_pages')),
        ('seotext_cms_pages', _(u'Seo_Text cms_pages')),

        ('title_cms_news', _(u'Title cms_news')),
        ('h1_cms_news', _(u'H1 cms_news')),
        ('keywords_cms_news', _(u'Keywords cms_news')),
        ('description_cms_news', _(u'Description cms_news')),
        ('seotext_cms_news', _(u'Seo_Text cms_news')),


        ('title_realty_section', _(u'Title realty_section')),
        ('title_realty_category', _(u'Title realty_category')),
        ('title_realty_country', _(u'Title realty_country')),
        ('title_realty_item', _(u'Title realty_item')),
        ('h1_realty_section', _(u'H1 realty_section')),
        ('h1_realty_category', _(u'H1 realty_category')),
        ('h1_realty_country', _(u'H1 realty_country')),
        ('h1_realty_item', _(u'H1 realty_item')),
        ('keywords_realty_section', _(u'Keywords realty_section')),
        ('keywords_realty_category', _(u'Keywords realty_category')),
        ('keywords_realty_country', _(u'Keywords realty_country')),
        ('keywords_realty_item', _(u'Keywords realty_item')),
        ('description_realty_section', _(u'Description realty_section')),
        ('description_realty_category', _(u'Description realty_category')),
        ('description_realty_country', _(u'Description realty_country')),
        ('description_realty_item', _(u'Description realty_item')),
        ('seotext_realty_section', _(u'Seo_Text realty_section')),
        ('seotext_realty_category', _(u'Seo_Text realty_category')),
        ('seotext_realty_country', _(u'Seo_Text realty_country')),
        ('seotext_realty_item', _(u'Seo_Text realty_item')),
    )

    name            = models.CharField(_(u"Название"), max_length=250,)
    seo_def         = models.CharField(_(u'Тип шаблона'), max_length=50, choices=TYPE_SEO, unique=True)
    template        = models.TextField(_(u"Шаблон"), help_text=_(u"Шаблоны автозамены:"
                                             u"<br> <b>__name__</b> - Название объекта,<br> <b>__country__</b> - Страна объекта,<br>"
                                             u"<b>__city__</b> - Город объекта,<br><b>__price__</b> - Цена объекта"))

    def __unicode__(self):
         return u"%s" % self.template

    class Meta:
        verbose_name = _(u'#Настройки СЕО')
        verbose_name_plural = _(u'#Настройки СЕО')
