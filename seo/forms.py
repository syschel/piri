# -*- coding: utf-8 -*-
import re
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db.models import Q

from seo.models import SeoDefault

class SeoForms(forms.ModelForm):
    """ Форма регистрации """

    def __init__(self, *args, **kwargs):
        super(SeoForms, self).__init__(*args, **kwargs)

    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #
    #     return cleaned_data

    class Meta:
        model = SeoDefault
        # exclude  = ('section', 'show',)
