# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SeoDefault.slug'
        db.alter_column(u'seo_seodefault', 'slug', self.gf('django.db.models.fields.CharField')(default=222, max_length=50))
        # Removing index on 'SeoDefault', fields ['slug']
        db.delete_index(u'seo_seodefault', ['slug'])


    def backwards(self, orm):
        # Adding index on 'SeoDefault', fields ['slug']
        db.create_index(u'seo_seodefault', ['slug'])


        # Changing field 'SeoDefault.slug'
        db.alter_column(u'seo_seodefault', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=255, null=True))

    models = {
        u'seo.seodefault': {
            'Meta': {'object_name': 'SeoDefault'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'seo_def': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'template': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['seo']