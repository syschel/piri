# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'SeoDefault.slug'
        db.delete_column(u'seo_seodefault', 'slug')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SeoDefault.slug'
        raise RuntimeError("Cannot reverse this migration. 'SeoDefault.slug' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'SeoDefault.slug'
        db.add_column(u'seo_seodefault', 'slug',
                      self.gf('django.db.models.fields.CharField')(max_length=50),
                      keep_default=False)


    models = {
        u'seo.seodefault': {
            'Meta': {'object_name': 'SeoDefault'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'seo_def': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'template': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['seo']