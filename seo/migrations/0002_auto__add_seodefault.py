# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SeoDefault'
        db.create_table(u'seo_seodefault', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('seo_def', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('template', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'seo', ['SeoDefault'])


    def backwards(self, orm):
        # Deleting model 'SeoDefault'
        db.delete_table(u'seo_seodefault')


    models = {
        u'seo.seodefault': {
            'Meta': {'object_name': 'SeoDefault'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'seo_def': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'template': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['seo']