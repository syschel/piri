# -*- coding: utf-8 -*-
import re
from seo.models import SeoDefault


def seo_replace(string, **kwargs):
    """
    Функция замены ключей
    :param string: строка
    :param obj: объект, откуда берём для подстановки

    __name__ - Название объекта
    __country__ - Страна объекта
    __city__ - - Город объекта
    __price___ - - Цена объекта

    """
    fields = ["__name__", "__country__", "__city__", "__price___"]
    for field in fields:
        if field in kwargs:
            string = string.replace(field, u"%s" % kwargs[field])

    string = re.sub("__.*?__", "", string)
    print "-= +++++ === =-"

    return string

def seo_default(seo_slug=False, obj=False, seo_data=False):
    """
    Генератор титле, кеев, десков и сеотекста для страниц сайта
    :param seo_slug: [Тип шаблона]
    :param obj: Объект для генерации настроек
    """

    # Дефотловые значения
    seo = {
        'h1': '',
        'title': '',
        'keywords': '',
        'description': '',
        'seo_text': '',
    }

    # Достаём значения по умолчанию
    default_slug = ['title_default', 'h1_default', 'keywords_default', 'description_default', 'seotext_default']
    seos_def    = SeoDefault.objects.filter(seo_def__in=default_slug)
    seos_template_def  = {
        'h1': '',
        'title': '',
        'keywords': '',
        'description': '',
        'seo_text': '',
    }
    for sseo in seos_def:
        if 'title_' in sseo.seo_def:
            seos_template_def['title'] = sseo.template
        if 'h1_' in sseo.seo_def:
            seos_template_def['h1'] = sseo.template
        if 'keywords_' in sseo.seo_def:
            seos_template_def['keywords'] = sseo.template
        if 'description_' in sseo.seo_def:
            seos_template_def['description'] = sseo.template
        if 'seotext_' in sseo.seo_def:
            seos_template_def['seotext'] = sseo.template

    # Достаём значения к текущему разделу
    seos_template = {
            'h1': '',
            'title': '',
            'keywords': '',
            'description': '',
            'seo_text': '',
        }
    if seo_slug:
        seos        = SeoDefault.objects.filter(seo_def__in=seo_slug)
        for sseo in seos:
            if 'title_' in sseo.seo_def:
                seos_template['title'] = sseo.template
            if 'h1_' in sseo.seo_def:
                seos_template['h1'] = sseo.template
            if 'keywords_' in sseo.seo_def:
                seos_template['keywords'] = sseo.template
            if 'description_' in sseo.seo_def:
                seos_template['description'] = sseo.template
            if 'seotext_' in sseo.seo_def:
                seos_template['seotext'] = sseo.template

    # Заполняем список из объекта
    # Так как я лошара и не смог вкурить адекватную проверку как в объекте модели проверить есть ли там искомое поле, то всё делаю через жопу, то есть через TRY/EXCEPT
    try:
        if obj.h1:
            seo['h1'] = obj.h1
        else:
            raise
    except:
        seo['h1'] = seos_template['h1'] if seos_template['h1'] else seos_template_def['h1']

    try:
        if obj.title:
            seo['title'] = obj.title
        else:
            raise
    except:
        seo['title'] = seos_template['title'] if seos_template['title'] else seos_template_def['title']

    try:
        if obj.keywords:
            seo['keywords'] = obj.keywords
        else:
            raise
    except:
        seo['keywords'] = seos_template['keywords'] if seos_template['keywords'] else seos_template_def['keywords']

    try:
        if obj.description:
            seo['description'] = obj.description
        else:
            raise
    except:
        seo['description'] = seos_template['description'] if seos_template['description'] else seos_template_def['description']

    try:
        if obj.seo_text:
            seo['seo_text'] = obj.seo_text
        else:
            raise
    except:
        seo['seo_text'] = seos_template['seo_text'] if seos_template['seo_text'] else seos_template_def['seo_text']

    # Заменяю строки с ключами, на данные из объекта
    if seo_data:
        seo_base = {
            'h1': seo_replace(seo['h1'], **seo_data),
            'title': seo_replace(seo['title'], **seo_data),
            'keywords': seo_replace(seo['keywords'], **seo_data),
            'description': seo_replace(seo['description'], **seo_data),
            'seo_text': seo_replace(seo['seo_text'], **seo_data),
        }
    else:
        seo_base = seo

    return seo_base

def parser_sletat():
    """
    Выкачиваем файл со списками слетать
    Парсим его и заносим изменения в БД
    """
    # А гори оно огнём!!! :-)
    return True