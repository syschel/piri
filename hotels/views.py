#-*- coding:utf-8 -*-
import re

# from django.contrib.auth.decorators import login_required
#
#
# from django.views.decorators.csrf import csrf_exempt, csrf_protect
# from django.utils.translation import ugettext_lazy as _
#
# from django.http import HttpResponseRedirect
# from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

# from django.db.models.query_utils import Q
#
# from hotels.models import *
# from hotels.forms import *


@render_to('hotels/index.html')
def hotels_index(request):
    return {'request': request}