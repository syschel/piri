# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('hotels.views',
    url(r'^$', 'hotels_index', name='hotels_index'),

)