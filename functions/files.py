# -*- coding: utf-8 -*-
import os
import uuid


def get_file_path(instance, filename, small_image=False):
    """
    Изменяем имя файла и уникализируем путь до него
    :param instance: Объект из которого была запущена функция
    :param filename: Текущее имя файла. Нужно только для расширения
    :return: Имя файла с путём до директории
    """

    # Переименовываем файл
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)

    # Определяем от какого класса у нас загрузка файла
    name = instance.__class__.__name__
    if small_image:
        name = name +'/small'

    return os.path.join("upload/class/" + name, filename)

def get_file_path_small(instance, filename):
    return get_file_path(instance, filename, small_image=True)