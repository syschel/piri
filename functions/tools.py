# -*- coding: utf-8 -*-


def gen_page_list(page_number, page_count):
    """
    Генерируем паджинг вида
    << 1..4,5,[6],7,8..160 >>
    В темплайте такое сотворить не удалось
    """
    my_page = []
    if page_count > 10:
        if page_number <= 4:
            for key in range(1, 7):
                my_page.append(key)
            my_page.append(u"...")
            my_page.append(page_count)

        elif page_number >= (page_count-4):
            my_page.append(1)
            my_page.append(u"...")
            for key in range((page_count-5), page_count+1):
                my_page.append(key)
        else:
            my_page.append(1)
            my_page.append(u"...")

            for key in range((page_number-2), (page_number+2)):
                my_page.append(key)

            my_page.append(u"...")
            my_page.append(page_count)
    else:
        for key in range(0, page_count):
            my_page.append(key+1)

    return my_page
