#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse

from cms.models import Section

def md5(str_hash = "hash text"):
    import hashlib
    hsh = hashlib.md5()
    hsh.update(str_hash)
    hash = hsh.hexdigest()
    return hash


class Faq(models.Model):
    """ Вопрос-ответ для каждого раздела """

    section        = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная запись"), null=True)
    subject        = models.CharField(_(u'#Название'), max_length=255)
    question       = HTMLField(_(u'#Текст Вопроса'), null=True, blank=True)
    reply          = HTMLField(_(u'#Текст Ответа'), null=True, blank=True)

    sort           = models.IntegerField(_(u"#Порядок сортировки"), default=1,)
    show           = models.BooleanField(_(u'#Показывать'), default=True)
    created        = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)


    def __unicode__(self):
        return u"%s" % self.subject

    def get_absolute_url(self):
        return reverse('feedback_faq', args=[self.pk])

    class Meta:
        verbose_name = _(u'#FAQ')
        verbose_name_plural = _(u'#FAQ')

class Form(models.Model):
    """ Форма вопроса с сайта для каждого раздела """

    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная запись"), null=True)
    subject         = models.CharField(_(u'#Тема'), max_length=255)
    email           = models.EmailField(_(u'#Емайд'), max_length=255)
    phone           = models.CharField(_(u'#Телефон'), max_length=255, null=True, blank=True)
    text            = models.TextField(_(u'#Текст'), null=True, blank=True)

    show           = models.BooleanField(_(u'#Показывать'), default=True)
    created        = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)


    def __unicode__(self):
        return u"%s" % self.subject


    class Meta:
        verbose_name = _(u'#Письма с сайта')
        verbose_name_plural = _(u'#Письма с сайта')


class Phone(models.Model):
    """ Форма Заказа звонка """

    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная запись"), null=True)
    phone           = models.CharField(_(u'#Телефон'), max_length=255)

    created        = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)


    def __unicode__(self):
        return u"%s" % self.phone


    class Meta:
        verbose_name = _(u'#Заказ звонка')
        verbose_name_plural = _(u'#Заказ звонка')

class SettingEmail(models.Model):
    """ Настройка емайлов манагеров, куда отправлять уведомления """

    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная запись"), null=True)
    email           = models.EmailField(_(u'#Емайл'), max_length=255)

    created        = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.email

    class Meta:
        verbose_name = _(u'#Настройки емайлов')
        verbose_name_plural = _(u'#Настройки емайлов')

class SettingSection(models.Model):
    """ Форма вопроса с сайта для каждого раздела """
    TOURS = 'tours'
    HOTELS = 'hotels'
    REALTY = 'realty'
    MODULES = (
        (TOURS, _(u'#Туры')),
        (HOTELS, _(u'#Отели')),
        (REALTY, _(u'#Недвижимость'))
    )

    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы в меню"), help_text=_(u"Выбрать раздел к которому относится данная запись"), null=True)
    modules         = models.CharField(_(u'#Програмный модуль'), choices=MODULES, max_length=10)

    created        = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.modules

    class Meta:
        verbose_name = _(u'#Настройки Где отображать')
        verbose_name_plural = _(u'#Настройки Где отображать')
        unique_together = ('section', 'modules',)