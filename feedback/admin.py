# -*- coding: utf-8 -*-
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib import admin

from feedback.models import Faq, Form, Phone, SettingEmail, SettingSection



class FaqAdmin(admin.ModelAdmin):
    list_display = ('subject', 'section', 'sort', 'show', 'created',)
    list_filter = ('section','show',)
    search_fields = ('subject', 'question', 'reply',)
    ordering = ('section', 'sort', 'subject',)
    list_editable = ("sort",)

class FormAdmin(admin.ModelAdmin):
    list_display = ('section', 'subject', 'email', 'phone', 'created',)
    list_display_links = ('section','subject', 'email', 'phone',)
    list_filter = ('section',)
    search_fields = ('subject', 'email', 'phone',)
    ordering = ('section', '-created',)

class PhoneAdmin(admin.ModelAdmin):
    list_display = ('section', 'phone', 'created',)
    list_display_links = ('section','phone',)
    list_filter = ('section',)
    search_fields = ('email', 'phone',)
    ordering = ('section', '-created',)

class SettingEmailAdmin(admin.ModelAdmin):
    list_display = ('section', 'email', 'created',)
    list_display_links = ('section','email',)
    list_filter = ('section',)

class SettingSectionAdmin(admin.ModelAdmin):
    list_display = ('section', 'modules', 'created',)
    list_display_links = ('section','modules',)
    list_filter = ('section',)

admin.site.register(Faq, FaqAdmin)
admin.site.register(Form, FormAdmin)
admin.site.register(Phone, PhoneAdmin)
admin.site.register(SettingEmail, SettingEmailAdmin)
admin.site.register(SettingSection, SettingSectionAdmin)
