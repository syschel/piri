#-*- coding:utf-8 -*-
import re

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

from django.db.models.query_utils import Q
from django.core.mail import send_mail
#
from feedback.models import Faq, SettingEmail
from feedback.forms import FeedbackFormForms, PhoneForms
from cms.models import Section


@render_to('feedback/index.html')
def feedback(request):
    url = request.META.get('PATH_INFO').split('/')[1:2]
    if url:
        url = url[0]
    return {'request': request, 'url':url}

@render_to('feedback/faq.html')
def faq(request):
    questions = Faq.objects.filter(show=True).order_by('-sort')
    return {'request': request, 'questions': questions}

@render_to('feedback/form.html')
def forms(request):
    if request.method == 'POST':
        url = request.META.get('PATH_INFO').split('/')[1:2]
        if url:
            url = url[0]
        section = Section.objects.filter(url=url, show=True, site=request.site_current).order_by("sort")
        if section:
            section = section[0]
        emails = SettingEmail.objects.filter(section=section).values_list("email", flat=True)
        if not emails:
            emails = ['robots@pirireis.ru']


        form = FeedbackFormForms(request.POST)
        if form.is_valid():
            some_form = form.save(commit=False)
            some_form.section = section
            some_form.save()
            send_mail(_(u'Письмо с сайта пирирейс: форма'), some_form.text, some_form.email, emails, fail_silently=False)
            return HttpResponseRedirect(reverse("feedback_form", args=[url]))
    else:
        form = FeedbackFormForms()
    return {'request': request, 'form': form}


@render_to('feedback/phone.html')
def phone(request):
    if request.method == 'POST':
        url = request.META.get('PATH_INFO').split('/')[1:2]
        if url:
            url = url[0]
        section = Section.objects.filter(url=url, show=True, site=request.site_current).order_by("sort")
        if section:
            section = section[0]
        emails = SettingEmail.objects.filter(section=section).values_list("email", flat=True)
        if not emails:
            emails = ['robots@pirireis.ru']
        form = PhoneForms(request.POST)
        if form.is_valid():
            some_form = form.save(commit=False)
            some_form.section = section
            some_form.save()
            send_mail(_(u'Письмо с сайта пирирейс: телефон'), some_form.phone, 'robots@pirireis.ru', emails, fail_silently=False)
            return HttpResponseRedirect(reverse("feedback_phone", args=[url]))
    else:
        form = PhoneForms()
    return {'request': request, 'form': form}