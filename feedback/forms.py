# -*- coding: utf-8 -*-
import re
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db.models import Q

from feedback.models import Faq, Form, Phone

class FeedbackFormForms(forms.ModelForm):
    """ Форма регистрации """

    def __init__(self, *args, **kwargs):
        super(FeedbackFormForms, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        if cleaned_data.get('phone'):
            phones = re.sub(u"[^0-9+]*", "", cleaned_data.get('phone'))
            phones = re.sub(u"^8", "+7", phones)
            if len(phones) < 5:
                raise forms.ValidationError(_(u'#Вы указали не верный формат телефона') + '%s' % phones)
            else:
                cleaned_data['phone'] = phones
        return cleaned_data

    class Meta:
        model = Form
        exclude  = ('section', 'show',)



class PhoneForms(forms.ModelForm):
    """ Форма регистрации """

    def __init__(self, *args, **kwargs):
        super(PhoneForms, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        if cleaned_data.get('phone'):
            phones = re.sub(u"[^0-9+]*", "", cleaned_data.get('phone'))
            phones = re.sub(u"^8", "+7", phones)
            if len(phones) < 5:
                raise forms.ValidationError(_(u'#Вы указали не верный формат телефона'))
            else:
                cleaned_data['phone'] = phones
        return cleaned_data

    class Meta:
        model = Phone
        exclude  = ('section',)