# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('feedback.views',
    url(r'^$', 'feedback', name='feedback'),
    url(r'^faq/$', 'faq', name='feedback_faq'),
    url(r'^form/$', 'forms', name='feedback_form'),
    url(r'^phone/$', 'phone', name='feedback_phone'),
)