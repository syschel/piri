#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse
from PIL import Image

from functions.files import get_file_path
from commons.models import Country, Communications

class Category(models.Model):
    """ Форма вопроса с сайта для каждого раздела """

    name            = models.CharField(_(u'#Название'), max_length=255, help_text=_(u"Название раздела выводимое в меню"))
    url             = models.CharField(_(u'#Ссылка'), max_length=255, blank=True, null=True)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0, blank=True,)

    h1               = models.CharField(_(u'#Заголовок страницы H1'), max_length=255, blank=True, null=True)
    title           = models.CharField(_(u'#Заголовок'), max_length=255, blank=True, null=True)
    keywords        = models.CharField(_(u'#Ключевые'), max_length=255, blank=True, null=True)
    description     = models.CharField(_(u'#Описание'), max_length=255, blank=True, null=True)
    seo_text        = HTMLField(_(u'#СЕО-текст'), null=True, blank=True)

    show            = models.BooleanField(_(u'#Показывать'), default=True)
    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('realty_category', args=[self.pk])

    class Meta:
        verbose_name = _(u'#Категории')
        verbose_name_plural = _(u'#Категории')
        ordering = ['sort',]


class Item(models.Model):
    """ Карточка товара (недвижимость) """

    SALE, RENT = range(0,2)
    TYPE_SALE = (
        (SALE, _(u'Продажа')),
        (RENT, _(u'Аренда')),
    )

    name            = models.CharField(_(u'#Название'), max_length=255, help_text=_(u"Название товара"))
    category        = models.ForeignKey(Category, verbose_name=_(u"Категория"), help_text=_(u"Раздел где выводится товар"), related_name="category")
    country         = models.ForeignKey(Country, verbose_name=_(u"Страна"), help_text=_(u"Страна товара"), related_name="item_category")
    price           = models.IntegerField(_(u"#Цена"), default=0,)
    type_sale       = models.IntegerField(_(u'#Тип продажи'), max_length=1, blank=True, default=SALE, choices=TYPE_SALE)
    hidden          = models.TextField(_(u'#Скрытое поле'), blank=True, null=True, help_text=_(u"Не выводится на сайт")) # ALTER TABLE `realty_item` ADD `hidden` LONGTEXT NOT NULL

    #itemchars       = models.ForeignKey('ItemChars', verbose_name=_(u"Характеристики товара"), blank=True, null=True, related_name="itemchars")
    #itemphoto       = models.ManyToManyField('ItemPhoto', verbose_name=_(u"Фотографии товара"), blank=True, null=True, related_name="itemphoto")

    show            = models.BooleanField(_(u'#Показывать'), default=True)
    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __setattr__(self, name, value):
        return super(Item, self).__setattr__(name, value)

    def get_absolute_url(self):
        return reverse('realty_item', args=[self.category_id, self.pk])

    def __unicode__(self):
        return u"%s: %s" % (self.name, self.pk)

    class Meta:
        verbose_name = _(u'#Товар')
        verbose_name_plural = _(u'#Товары')
        ordering = ['-pk',]

class ItemPhoto(models.Model):
    """ Фотографии товара """

    item            = models.ForeignKey(Item, verbose_name=_(u"Товар"), help_text=_(u"Связь с товаром"), blank=True, null=True, related_name="item_photo")
    big_image       = models.ImageField(_(u"большое изображение"), upload_to=get_file_path)
    main            = models.BooleanField(_(u'#Главная картинка'), default=False)

    def __unicode__(self):
        return self.pk

    def save(self, size_big=(500, 500), *args, **kwargs):
        # Ресайзим картинку
        super(ItemPhoto, self).save() #Вроде бы стоит супер вниз убрать, чтобы дважды не мучало файл
        # большую тоже сресайзим
        if self.big_image:
            filename = self.big_image.path
            image = Image.open(filename)
            image.thumbnail(size_big, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u"#Изображение товара"
        verbose_name_plural = u"#Изображения товаров"
        #unique_together = (('item', 'show'))

class ItemChars(models.Model):
    """
    Характеристики товара
    """
    SECOND00, SECOND01, SECOND02 = range(0,3)
    CHAR04 = (
        (SECOND00, _(u"Строящееся")),
        (SECOND01, _(u"Новостройка")),
        (SECOND02, _(u"Вторичное")),
    )

    LOCAT00, LOCAT01, LOCAT02, LOCAT03 = range(0,4)
    CHAR06 = (
        (LOCAT00, _(u"Около моря")),
        (LOCAT01, _(u"Около гор")),
        (LOCAT02, _(u"В пустыне")),
        (LOCAT03, _(u"Лесная местность")),
    )

    NO, YES, PARTLY = range(0,3)
    CHAR07 = (
        (NO, _(u"Нет")),
        (YES, _(u"Да")),
        (PARTLY, _(u"Частично")),
    )
    CHAR10 = (
        (NO, _(u"Нет")),
        (YES, _(u"Да")),
    )
    CHAR13 = (
        (NO, _(u"Нет")),
        (YES, _(u"Да")),
    )
    CHAR14 = (
        (NO, _(u"Нет")),
        (YES, _(u"Да")),
    )

    REAL00, REAL01, REAL02, REAL03, REAL04, REAL05, REAL06, REAL07, REAL08 = range(0,9)
    CHAR16 = (
        (REAL00, _(u"Отель")),
        (REAL01, _(u"Ресторан")),
        (REAL02, _(u"Стоянка")),
        (REAL03, _(u"Офисные помещения")),
        (REAL04, _(u"Склады")),
        (REAL05, _(u"Курортные комплексы")),
        (REAL06, _(u"Магазины")),
        (REAL07, _(u"Доходные дома")),
        (REAL08, _(u"Студенческое жилье")),
    )

    item                       = models.ForeignKey(Item, verbose_name=_(u"Товар"), help_text=_(u"Связь с товаром"), blank=True, null=True, related_name="item_chars")
    chars01                    = models.CharField(_(u'#Количество комнат'), max_length=255, blank=True, null=True)
    chars02                    = models.CharField(_(u'#Общая площадь м2'), max_length=255, blank=True, null=True)
    chars12                    = models.CharField(_(u'#Жилая площадь м2'), max_length=255, blank=True, null=True) # ALTER TABLE `realty_itemchars` CHANGE `chars12` `chars12` VARCHAR( 255 ) NULL DEFAULT NULL
    chars03                    = models.CharField(_(u'#Количество сан узлов'), max_length=255, blank=True, null=True)
    chars04                    = models.IntegerField(_(u'#Тип'), max_length=1, blank=True, null=True, default=SECOND02, choices=CHAR04, help_text=_(u"Строящееся, новостройка, вторичное"))
    chars05                    = models.CharField(_(u'#Регион'), max_length=255, blank=True, null=True)
    chars06                    = models.IntegerField(_(u'#Расположение'), max_length=1, blank=True, null=True, default=LOCAT00, choices=CHAR06,)
    chars07                    = models.IntegerField(_(u'#Мебелирование'), max_length=1, blank=True, null=True, default=NO, choices=CHAR07,)
    chars08                    = models.IntegerField(_(u'#Всего этажей'), default=0, blank=True, null=True)
    chars09                    = models.IntegerField(_(u'#Этаж'), default=0, blank=True, null=True)
    chars10                    = models.IntegerField(_(u'#Балкон'), max_length=1, blank=True, null=True, default=NO, choices=CHAR10,)
    chars11                    = HTMLField(_(u'#Подробное описание'), blank=True, null=True)

    chars13                    = models.IntegerField(_(u'#Гараж'), max_length=1, blank=True, null=True, default=NO, choices=CHAR13,)
    chars14                    = models.IntegerField(_(u'#Легализация'), max_length=1, blank=True, null=True, default=NO, choices=CHAR14, help_text=_(u"Легализация под строительство"))
    chars15                    = models.ManyToManyField(Communications, verbose_name=_(u"Коммуникации"), null=True, blank=True)

    chars16                    = models.IntegerField(_(u'#Тип комерческой недвижимости'), max_length=255, blank=True, null=True, choices=CHAR16)
    chars17                    = models.IntegerField(_(u'#Рентабильность'), default=0, blank=True, null=True)



    def __unicode__(self):
        return u"Характеристики товара: %s" % self.pk

    class Meta:
        verbose_name = _(u'#Характеристика товара')
        verbose_name_plural = _(u'#Характеристики товара')

class OrdersForm(models.Model):
    """
    Форма заявок с сайта
    """
    item                    = models.ForeignKey(Item, verbose_name=_(u"Товар"), help_text=_(u"Связь с товаром"), blank=True, null=True, related_name="item_ordersform")
    fio                     = models.CharField(_(u'#ФИО'), max_length=255, blank=True, null=True)
    phone                   = models.CharField(_(u'#Телефон'), max_length=255, blank=True, null=True)
    email                   = models.EmailField(_(u'#Емайл'), max_length=255, blank=True, null=True)
    messages                = models.TextField(_(u'#Контакты в подвале сайта'), null=True, blank=True)
    created                 = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s: %s" % (self.item.name, self.pk)

    class Meta:
        verbose_name = _(u'#Заявоки форма')
        verbose_name_plural = _(u'#Заявоки форма')
        ordering = ['-created',]
