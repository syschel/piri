# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('realty.views',
    url(r'^$', 'index', name='realty_index'),
    url(r'^page_(?P<page_id>\d+)/$', 'index', name='realty_index_page'),
    url(r'^cat_(?P<id>\d+)/$', 'category', name='realty_category'),
    url(r'^cat_(?P<id>\d+)/num_(?P<page_id>\d+)/$', 'category', name='realty_category'),
    url(r'^reg_(?P<id>\d+)/$', 'country', name='realty_country'),
    url(r'^reg_(?P<id>\d+)/num_(?P<page_id>\d+)/$', 'country', name='realty_country'),
    url(r'^cat_(?P<cat_id>\d+)/(?P<item_id>\d+)/', 'item', name='realty_item'),
    url(r'^order_form/$', 'order_form', name='realty_order_form'),
)