# -*- coding: utf-8 -*-
import re
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db.models import Q

from realty.models import OrdersForm

class OrdersFormForms(forms.ModelForm):
    """ Форма регистрации """

    def __init__(self, *args, **kwargs):
        super(OrdersFormForms, self).__init__(*args, **kwargs)


    def clean_fio(self):
        cleaned_data = self.cleaned_data.get('fio')

        if len(cleaned_data) < 3:
            raise forms.ValidationError(_(u'Минимум 3 символа'))
        return cleaned_data

    def clean_phone(self):
        cleaned_data = self.cleaned_data
        if cleaned_data:
            phones = re.sub(u"[^0-9+]*", "", cleaned_data.get('phone'))
            phones = re.sub(u"^8", "+7", phones)
            if len(phones) < 5:
                cleaned_data['phone'] = None
                raise forms.ValidationError(_(u'#Вы указали не верный формат телефона'))
            else:
                cleaned_data = phones
        return cleaned_data

    class Meta:
        model = OrdersForm