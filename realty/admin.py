#-*- coding:utf-8 -*-
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib import admin
from django.contrib.contenttypes import generic

from django import forms


from realty.models import Category, Item, ItemPhoto, ItemChars, OrdersForm


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'sort', 'show', 'created',)
    list_display_links = ('name','url',)
    list_filter = ('show',)
    search_fields = ['name',]
    ordering = ('sort',)
    list_editable = ("sort",)

class ItemCharsInline(admin.StackedInline):
    model = ItemChars
    extra = 1
    max_num = 1


class ItemPhotoInline(admin.TabularInline):
    model = ItemPhoto
    extra = 1


class ItemyAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'country', 'price', 'type_sale', 'show', 'created',)
    list_display_links = ('name', 'category', 'country', 'price', 'type_sale',)
    list_filter = ('show',)
    search_fields = ['name',]
    ordering = ('created',)
    date_hierarchy = 'created'
    #exclude = ['itemchars', 'itemphoto']

    inlines = [ItemCharsInline, ItemPhotoInline]



class ItemPhotoAdmin(admin.ModelAdmin):
    list_display = ('big_image',)
    list_display_links = ('big_image',)
    inlines = [ItemPhotoInline]


class OrdersFormAdmin(admin.ModelAdmin):
    list_display = ('pk', 'item', 'fio', 'phone', 'email', 'created')
    list_display_links = ('pk', 'item', 'fio', 'phone', 'email',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemyAdmin)
#admin.site.register(ItemPhoto, ItemPhotoAdmin)
#admin.site.register(ItemChars)
admin.site.register(OrdersForm, OrdersFormAdmin)