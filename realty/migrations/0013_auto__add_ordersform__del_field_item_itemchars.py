# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'OrdersForm'
        db.create_table(u'realty_ordersform', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='item_ordersform', null=True, to=orm['realty.Item'])),
            ('fio', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('messages', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'realty', ['OrdersForm'])

        # Deleting field 'Item.itemchars'
        db.delete_column(u'realty_item', 'itemchars_id')

        # Removing M2M table for field itemphoto on 'Item'
        db.delete_table(db.shorten_name(u'realty_item_itemphoto'))


    def backwards(self, orm):
        # Deleting model 'OrdersForm'
        db.delete_table(u'realty_ordersform')

        # Adding field 'Item.itemchars'
        db.add_column(u'realty_item', 'itemchars',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='itemchars', null=True, to=orm['realty.ItemChars'], blank=True),
                      keep_default=False)

        # Adding M2M table for field itemphoto on 'Item'
        m2m_table_name = db.shorten_name(u'realty_item_itemphoto')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('item', models.ForeignKey(orm[u'realty.item'], null=False)),
            ('itemphoto', models.ForeignKey(orm[u'realty.itemphoto'], null=False))
        ))
        db.create_unique(m2m_table_name, ['item_id', 'itemphoto_id'])


    models = {
        u'commons.communications': {
            'Meta': {'ordering': "['sort']", 'object_name': 'Communications'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        u'commons.country': {
            'Meta': {'object_name': 'Country'},
            'height': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'width': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3', 'blank': 'True'})
        },
        u'realty.category': {
            'Meta': {'ordering': "['sort']", 'object_name': 'Category'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'seo_text': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'realty.item': {
            'Meta': {'object_name': 'Item'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category'", 'to': u"orm['realty.Category']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commons.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'type_sale': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'blank': 'True'})
        },
        u'realty.itemchars': {
            'Meta': {'object_name': 'ItemChars'},
            'chars01': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'chars02': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'chars03': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'chars04': ('django.db.models.fields.IntegerField', [], {'default': '2', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars05': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'chars06': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars07': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars08': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'chars09': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'chars10': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars11': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'chars12': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'chars13': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars14': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'chars15': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['commons.Communications']", 'null': 'True', 'blank': 'True'}),
            'chars16': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'chars17': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'item_chars'", 'null': 'True', 'to': u"orm['realty.Item']"})
        },
        u'realty.itemphoto': {
            'Meta': {'object_name': 'ItemPhoto'},
            'big_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'item_photo'", 'null': 'True', 'to': u"orm['realty.Item']"}),
            'small_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'realty.ordersform': {
            'Meta': {'object_name': 'OrdersForm'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'item_ordersform'", 'null': 'True', 'to': u"orm['realty.Item']"}),
            'messages': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['realty']