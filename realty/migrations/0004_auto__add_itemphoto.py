# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ItemPhoto'
        db.create_table(u'realty_itemphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('small_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('big_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'realty', ['ItemPhoto'])


    def backwards(self, orm):
        # Deleting model 'ItemPhoto'
        db.delete_table(u'realty_itemphoto')


    models = {
        u'commons.country': {
            'Meta': {'object_name': 'Country'},
            'height': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'width': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3'})
        },
        u'realty.category': {
            'Meta': {'object_name': 'Category'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'seo_text': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'realty.item': {
            'Meta': {'object_name': 'Item'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['realty.Category']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['commons.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'type_sale': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '1', 'blank': 'True'})
        },
        u'realty.itemphoto': {
            'Meta': {'object_name': 'ItemPhoto'},
            'big_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'small_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['realty']