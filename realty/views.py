# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404

import math

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

from functions.tools import gen_page_list
from seo.tools import seo_default, seo_replace
from realty.models import Category, Item, ItemChars, ItemPhoto
from realty.forms import OrdersFormForms
from commons.models import Country
from cms.models import Section

@render_to('realty/index.html')
def index(request, page_id=1):
    """
    Главная страница недвижимости
    """
    hosts = request.META['PATH_INFO'].split("/")
    if hosts:
        section = Section.objects.filter(url=hosts[1])
        if section:
            section = section[0]
            seo = seo_default(seo_slug=['title_cms_section', 'h1_cms_section', 'keywords_cms_section', 'description_cms_section', 'seotext_cms_section'], obj=section)

    #category = Category.objects.none()
    category = Category.objects.filter(show=True)
    country = Country.objects.filter(show=True)
    items_list = Item.objects.filter(show=True)
    item_pages = 10 # количество товаров на страницу
    page = int(page_id) # текущая страница


    paginator = Paginator(items_list, item_pages)
    page_count = items_list.count()
    items_rows = int(math.ceil(float(page_count) / float(item_pages))) # Сколько страниц получится
    pages_list = gen_page_list(int(page), int(items_rows))

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    for item in items:
        item.itemchars = item.item_chars.all()[0]
        item.itemphoto = item.item_photo.all()[0] if item.item_photo.all() else None

    return {'request':request, 'category':category, 'country':country, 'items':items, 'pages_list': pages_list, 'page_id': int(page_id), 'seo':seo}

@render_to('realty/category.html')
def category(request, id, page_id=1):
    """
    Вывод выбраной категории
    """
    category = get_object_or_404(Category, pk=id)
    seo_data = {
        "__name__": category.name,
        "__country__": "",
        "__city__": "",
        "__price___": "",
    }
    seo = seo_default(seo_slug=['title_realty_category', 'h1_realty_category', 'keywords_realty_category', 'description_realty_category', 'seotext_realty_category'], obj=category, seo_data=seo_data)





    #hosts = request.META['PATH_INFO'].split("/")
    #if hosts:
    #    section = Section.objects.filter(url=hosts[1])
    #    if section:
    #        section = section[0]
    #
    #        seo = {
    #            'h1': category.h1 if category.h1 else category.title,
    #            'title': category.title,
    #            'keywords': category.keywords,
    #            'description': category.description,
    #            'seo_text': category.seo_text,
    #        }

    items_list = Item.objects.filter(show=True, category=id)
    item_pages = 10 # количество товаров на страницу
    page = int(page_id) # текущая страница


    paginator = Paginator(items_list, item_pages)
    page_count = items_list.count()
    items_rows = int(math.ceil(float(page_count) / float(item_pages))) # Сколько страниц получится
    pages_list = gen_page_list(int(page), int(items_rows))

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)

    for item in items:
        item.itemchars = item.item_chars.all()[0]
        item.itemphoto = item.item_photo.all()[0] if item.item_photo.all() else None
    type_page = 'category'
    return {'request':request, 'items':items, 'category':category, 'pages_list': pages_list, 'page_id': int(page_id), 'type_page':type_page, 'seo':seo}

@render_to('realty/category.html')
def country(request, id, page_id=1):
    """
    Вывод выбраной категории
    """
    category = get_object_or_404(Country, pk=id)

    hosts = request.META['PATH_INFO'].split("/")
    if hosts:
        section = Section.objects.filter(url=hosts[1])
        if section:
            section = section[0]
            seo = seo_default(seo_slug=['title_realty_country', 'h1_realty_country', 'keywords_realty_country', 'description_realty_country', 'seotext_realty_country'], obj=section)


    items_list = Item.objects.filter(show=True, country=id)
    item_pages = 10 # количество товаров на страницу
    page = int(page_id) # текущая страница

    paginator = Paginator(items_list, item_pages)
    page_count = items_list.count()
    items_rows = int(math.ceil(float(page_count) / float(item_pages))) # Сколько страниц получится
    pages_list = gen_page_list(int(page), int(items_rows))
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)


    for item in items:
        item.itemchars = item.item_chars.all()[0]
        item.itemphoto = item.item_photo.all()[0] if item.item_photo.all() else None
    type_page = 'country'
    return {'request':request, 'items':items, 'category':category, 'pages_list': pages_list, 'page_id': int(page_id), 'type_page':type_page, 'seo':seo}

@render_to('realty/item.html')
def item(request, item_id, cat_id=False):
    """
    Вывод карточки товара
    """

    item = get_object_or_404(Item, pk=item_id)
    item.itemchars = item.item_chars.all()
    if item.itemchars:
        item.itemchars = item.itemchars[0]
    item.itemphoto = item.item_photo.all()
    seo_data = {
        "__name__": item.name,
        "__country__": item.country,
        "__city__": "",
        "__price___": item.price,
    }
    seo = seo_default(seo_slug=['title_realty_item', 'h1_realty_item', 'keywords_realty_item', 'description_realty_item', 'seotext_realty_item'], obj=item, seo_data=seo_data)
    #seo = {
    #    'title': u"%s / %s" % (item.name, item.category.name),
    #    'keywords': item.name,
    #    'description': item.name,
    #}
    return {'request':request, 'item':item, 'seo':seo}

@ajax_request
def order_form(request):
    """
    Ajax форма заказа
    """
    if request.is_ajax:
        if request.method == 'POST':
            dates_info = request.POST
            if dates_info:
                form = OrdersFormForms(dates_info)
                if form.is_valid():
                    some_form = form.save(commit=False)
                    some_form.save()
                    data = u"%s" % _(u"Сообщение отправленно.")
                    return {'status': 'true', 'data': data}
                else:
                    return {'status': 'false', 'data': form.errors}
                result = "ssss"     #Country.objects.filter(title__contains=query)

    return {'status': 'error', 'message': 'Bad request'}
