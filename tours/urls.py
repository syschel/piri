# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns


urlpatterns = patterns('tours.views',
    url(r'^$', 'tours_index', name='tours_index'),
    url(r'^hot_tours/$', 'hot_tours', name='hot_tours'),
    url(r'^direct/$', 'direct', name='tours_direct'),

)