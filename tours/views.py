#-*- coding:utf-8 -*-
import re

from django.contrib.auth.decorators import login_required


from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from annoying.decorators import render_to, ajax_request

from django.db.models.query_utils import Q

#from tours.models import *
#from tours.forms import *

from seo.tools import seo_default

from cms.models import Section


@render_to('tours/index.html')
def tours_index(request):
    hosts = request.META['PATH_INFO'].split("/")
    if hosts:
        section = Section.objects.filter(url=hosts[1])
        if section:
            section = section[0]
            seo = seo_default(seo_slug=['title_cms_section', 'h1_cms_section', 'keywords_cms_section', 'description_cms_section', 'seotext_cms_section'], obj=section)
            #seo = {
            #    'h1': section.h1 if section.h1 else section.title,
            #    'title': section.title,
            #    'keywords': section.keywords,
            #    'description': section.description,
            #}
    return {'request': request, 'seo': seo}

@render_to('tours/hottours.html')
def hot_tours(request):


    seo = {
        'title': _(u'Горячие туры'),
        'keywords': _(u'Горячие туры'),
        'description': _(u'Горячие туры'),
    }
    return {'request': request, 'seo':seo}


@render_to('tours/direct.html')
def direct(request):


    seo = {
        'title': _(u'Горячие туры'),
        'keywords': _(u'Горячие туры'),
        'description': _(u'Горячие туры'),
    }
    return {'request': request, 'seo':seo}