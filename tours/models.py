#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models

def md5(str_hash = "hash text"):
    import hashlib
    hsh = hashlib.md5()
    hsh.update(str_hash)
    hash = hsh.hexdigest()
    return hash
