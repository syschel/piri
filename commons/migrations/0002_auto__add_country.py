# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'commons_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('width', self.gf('django.db.models.fields.CharField')(default=0, max_length=3)),
            ('height', self.gf('django.db.models.fields.CharField')(default=0, max_length=3)),
            ('show', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'commons', ['Country'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'commons_country')


    models = {
        u'commons.country': {
            'Meta': {'object_name': 'Country'},
            'height': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'width': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '3'})
        }
    }

    complete_apps = ['commons']