#-*- coding:utf-8 -*-
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib import admin

from commons.models import Country, Communications

class CountryAdmin(admin.ModelAdmin):
    list_display = ('title', 'title_en', 'show','width', 'height',)
    list_display_links = ('title','title_en',)
    list_filter = ('show',)
    list_editable = ('width', 'height',)

class CommunicationsAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort', 'show',)
    list_display_links = ('name',)
    list_filter = ('show',)
    list_editable = ("sort",)


admin.site.register(Country, CountryAdmin)
admin.site.register(Communications, CommunicationsAdmin)