#-*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse


class Country(models.Model):
    title           = models.CharField(_(u'#Название_RU'), max_length=100)
    title_en        = models.CharField(_(u'#Название_EN'), max_length=100)

    width           = models.CharField(_(u'#координата по width'), max_length=3, default=0, blank=True,)
    height          = models.CharField(_(u'#координата по height'), max_length=3, default=0, blank=True,)

    show            = models.BooleanField(_(u'#Показывать'), default=True)

    def name(self):
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u'#Страны')
        verbose_name_plural = _(u'#Страны')
        ordering = ['title',]

class Communications(models.Model):
    """

    """
    name            = models.CharField(_(u'#Название'), max_length=255)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0, blank=True,)
    show            = models.BooleanField(_(u'#Показывать'), default=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u'#Коммуникации')
        verbose_name_plural = _(u'#Коммуникации')
        ordering = ['sort',]