/**
 * Created with PyCharm.
 * User: ad
 * Date: 09.08.13
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */

$('input.voting_ajax').change(function() {

    var val = $(this).attr("value");
    var csrf = $(this).attr("csrf");

    if (val)
    {
        $.ajax({
            type: "POST",
            url: '/voting_add/',
            dataType: 'json',
            data: {
                'voting': val,
                'csrfmiddlewaretoken': csrf
                },
            success: function(msg){
                if(msg['status'] == true){
                    var arr = msg['result'];
                    for (var key in arr)
                    {
                        $("div#votingline_"+key).css("width", arr[key]+"px");
                        $("label input.voting_ajax").remove()
                    }
                    // Достанем число, чтобы его увеличить
                    var resul = Number($("strong#votingresult_"+val).html());
                    $("strong#votingresult_"+val).html(resul + 1);
                }
                else{
                    alert(msg['message']);
                }
            },
            error: function(err) {
                alert(JS_LANG['ajax_error']);
            }
        });

    }
    return false;
});

$.datepicker.setDefaults($.datepicker.regional['ru']);
$( ".datepicker" ).datepicker({
    changeMonth: true,
    altFormat: true,
    changeYear: true,
    regional: "fr",
    yearRange: "1900:2022"
});

$('h2.fq_subject').click(function(e){
    var rel = $(this).attr("rel");
    $('fieldset.'+rel).toggle()

    return false;
});
/* Недвижимость */
$('#item_modal a.closed').click(function(){
    $('#item_modal').hide("show");
    return false;
});
$('a.buy_realrty').click(function(){
    $('#item_modal').show("show");
    return false;
});

$('form.realty_form').submit(function(){
    var dates_info = $(this).serialize();
    $.ajax({
        type: "post",
        url: '/realty/order_form/',
        data: dates_info,
        dataType: 'json',
        success: function(msg){
            if(msg['status'] == 'true'){
                $('#item_modal .red').remove();
                $('.form_messages').html(msg['data']);
                $('.form_messages').addClass('green');
                $('.form_messages').show();
                $('#item_modal').delay(3000).hide('show');
            }else{
                $('#item_modal .red').remove();
                if (msg['data']['fio']){ $('input[name=fio]').after('<div class="red">' + msg['data']['fio'] + '</div>'); }
                if (msg['data']['phone']){ $('input[name=phone]').after('<div class="red">' + msg['data']['phone'] + '</div>'); }
                if (msg['data']['email']){ $('input[name=email]').after('<div class="red">' + msg['data']['email'] + '</div>'); }


            }

        },
        error: function() {
            alert('error ajax');
        }
    });
    return false;
});
