#-*- coding:utf-8 -*-

from django.contrib.sites.models import Site
from django.db.models import Q
from cms.models import Section

def site(request):
    #
    return {
        'site': Site.objects.get((Q(domain=request.META['HTTP_HOST']) | Q(sites__root=True)), Q(sites__show=True)),
        'sections': Section.objects.filter(site=request.site_current, show=True).order_by('sort')
    }
