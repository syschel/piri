#-*- coding:utf-8 -*-
import os, re

from django import template

from cms.models import Section, SubSection, Voting, Report, ResultVoting

register = template.Library()


@register.inclusion_tag('cms/top_menu.html', takes_context=True)
def top_menu(context):
    # Верхнее сквозное меню
    section = Section.objects.filter(show=True, site=context["request"].site_current).order_by("sort")
    my_url = re.sub(u"^/|/$", "", context["request"].META.get('PATH_INFO'))
    return {'sections': section, 'my_url':my_url}

@register.inclusion_tag('cms/sub_menu.html', takes_context=True)
def sub_menu(context):
    # Вспомогательное боковое меню
    section_name = context["request"].META.get('PATH_INFO').split('/')[1:2]
    if section_name:
        section_name = section_name[0]
    try:
        section_name = context["request"].subsection_url
    except:
        pass
    section = SubSection.objects.filter(section__url=section_name, show=True, site=context["request"].site_current).order_by("sort")
    my_url = context["request"].META.get('PATH_INFO')
    return {'sections': section, 'my_url':my_url, 'section_name':section_name}


@register.inclusion_tag('cms/voting/voting.html', takes_context=True)
def voting(context, slug):
    # Вывод опроса
    voting = Voting.objects.filter(show=True, section__pk=slug)

    ip_address = context["request"].META.get("REMOTE_ADDR", None)

    if voting:
        votings = voting[0]
        report = Report.objects.filter(voting=votings)
        result = ResultVoting.objects.filter(voting=votings)

        arr_res = {}
        sum = 0
        valid = True
        for resul in result:
            if resul.report_id in arr_res:
                arr_res[int(resul.report_id)] = arr_res[int(resul.report_id)] + 1
            else:
                arr_res[int(resul.report_id)] = 1

            # Проверяем голосовал ли этот IP в данном опросе
            if resul.ip_user == ip_address:
                valid = False

            sum += 1
        if sum:
            proc = 120 / sum
        else:
            proc = 0

        for rep in report:
            rep.count = arr_res.get(rep.pk, 0)
            rep.proc = proc * int(rep.count)

        return {'voting':votings, 'report':report, 'valid':valid,}
    else:
        return {}

@register.inclusion_tag('null.html', takes_context=True)
def section_text(context):
    # Верхнее сквозное меню
    section_name = context["request"].META.get('PATH_INFO').split('/')[1:2]
    if section_name:
        section_name = section_name[0]
    try:
        section_name = context["request"].subsection_url
    except:
        pass
    section = Section.objects.filter(url=section_name, show=True, site=context["request"].site_current).order_by("sort")
    if section:
        seo_text = section[0].seo_text
    else:
        seo_text = False


    return {'success':seo_text}