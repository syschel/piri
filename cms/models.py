#-*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from PIL import Image
from tinymce.models import HTMLField

from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

def md5(str_hash = "hash text"):
    import hashlib
    hsh = hashlib.md5()
    hsh.update(str_hash)
    hash = hsh.hexdigest()
    return hash

class Sites(Site):
    """ Профиль домена """
    menu_name       = models.CharField(_(u'#Название сайта для меню'), max_length=255, blank=True, null=True)
    about           = HTMLField(_(u'#Описание'), null=True, blank=True)
    meta            = models.TextField(_(u'#Настройки meta-тегов'), null=True, blank=True)
    robots          = models.TextField(_(u'#Настройки robots.txt'), null=True, blank=True)
    footer          = models.TextField(_(u'#Вывод коде перед </body>'), null=True, blank=True)
    contacts        = models.CharField(_(u'#Сквозной контактный телефон'), max_length=255, blank=True, null=True)
    footer_contacts = models.TextField(_(u'#Контакты в подвале сайта'), null=True, blank=True)
    root            = models.BooleanField(_(u'#Главный домен'), default=False)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0,)
    show            = models.BooleanField(_(u'#Включен'), default=True)
    sms_name        = models.CharField(_(u'#Имя для СМС'), max_length=15, blank=True, null=True)

    title            = models.CharField(_(u'#Заголовок главной'), max_length=255, blank=True, null=True)
    keywords         = models.CharField(_(u'#Ключевые главной'), max_length=255, blank=True, null=True)
    description      = models.CharField(_(u'#Описание главной'), max_length=255, blank=True, null=True)


    objects = models.Manager()
    on_site = CurrentSiteManager()


    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'#Сайты')
        verbose_name_plural = _(u'#Сайты')
        ordering = ("-sort",)


class Section(models.Model):
    """ Управление разделами сайта """

    name            = models.CharField(_(u'#Название'), max_length=255, help_text=_(u"Название раздела выводимое в главном меню"))
    url             = models.CharField(_(u'#Ссылка на раздел'), max_length=255,
                                       help_text=_(u"Одно слово на латинице и цифр, без пробелов. В разделах с глобальным модулем, настраивается программистом сайта."))
    show            = models.BooleanField(_(u'#Включен'), default=True)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0,)
    site            = models.ForeignKey(Sites, verbose_name=_(u"Сайт"), help_text=_(u"Выбрать домен к которому относится данный раздел"))
    modules         = models.ManyToManyField("Modules", verbose_name=_(u"Список подключенных модулей"), blank=True, null=True,)
    #SEO
    h1               = models.CharField(_(u'#Заголовок страницы H1'), max_length=255, blank=True, null=True)
    title            = models.CharField(_(u'#Заголовок окна'), max_length=255, blank=True, null=True)
    keywords         = models.CharField(_(u'#Ключевые слова'), max_length=255, blank=True, null=True)
    description      = models.CharField(_(u'#Описание раздела'), max_length=255, blank=True, null=True)
    seo_text         = HTMLField(_(u'#СЕО-текст раздела'), null=True, blank=True)

    #slug             = models.SlugField(_(u'#Ключ раздела'), help_text=_(u"Одно слово на латинице и цифр, без пробелов"), max_length=255)
    created          = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return u"/%s/" % self.url

    class Meta:
        verbose_name = _(u'#Разделы')
        verbose_name_plural = _(u'#Разделы')
        ordering = ("-sort",)

class SubSection(models.Model):
    """ Управление под разделами сайта """

    section         = models.ForeignKey(Section, verbose_name=_(u"Родительский раздел"), help_text=_(u"Выбрать раздел к которому относится данный подраздел"), null=True)
    name            = models.CharField(_(u'#Название'), max_length=255, help_text=_(u"Название раздела выводимое в главном меню"))
    url             = models.CharField(_(u'#Ссылка на раздел'), max_length=255,
                                       help_text=_(u"Одно слово на латинице и цифр, без пробелов. В разделах с глобальным модулем, настраивается программистом сайта."))
    show            = models.BooleanField(_(u'#Включен'), default=True)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0,)
    site            = models.ForeignKey(Sites, verbose_name=_(u"Сайт"), help_text=_(u"Выбрать домен к которому относится данный раздел"))
    modules         = models.ManyToManyField("Modules", verbose_name=_(u"Список подключенных модулей"), blank=True, null=True,)
    #SEO
    h1               = models.CharField(_(u'#Заголовок страницы H1'), max_length=255, blank=True, null=True)
    title            = models.CharField(_(u'#Заголовок окна'), max_length=255, blank=True, null=True)
    keywords         = models.CharField(_(u'#Ключевые слова'), max_length=255, blank=True, null=True)
    description      = models.CharField(_(u'#Описание раздела'), max_length=255, blank=True, null=True)
    seo_text         = HTMLField(_(u'#СЕО-текст раздела'), null=True, blank=True)

    #slug             = models.SlugField(_(u'#Ключ раздела'), help_text=_(u"Одно слово на латинице и цифр, без пробелов"), max_length=255, blank=True, null=True,)
    created          = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return u"/%s/%s/" % (self.section.url, self.url)

    class Meta:
        verbose_name = _(u'#Под разделы')
        verbose_name_plural = _(u'#Под разделы')
        ordering = ("-sort",)


class Modules(models.Model):
    """ Список зарегестрированных модулей сайта """
    name            = models.CharField(_(u'#Название'), max_length=255)
    #slug            = models.SlugField(_(u'#Ключ модуля'), help_text=_(u"Одно слово на латинице и цифр, без пробелов"), max_length=255)
    show            = models.BooleanField(_(u'#Включен'), default=True)
    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'#Модули')
        verbose_name_plural = _(u'#Модули')


class Pages(models.Model):
    """ Модуль управление страницами """
    name            = models.CharField(_(u'#Название'), max_length=255)
    url             = models.SlugField(_(u'#URL /pages/*'), help_text=_(u"Одно слово на латинице и цифр, без пробелов"), max_length=255)
    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная страница"), null=True, blank=True)
    show            = models.BooleanField(_(u'#Включен'), default=True)

    text            = HTMLField(_(u'#Текст раздела'), null=True, blank=True)

    #SEO
    h1              = models.CharField(_(u'#Заголовок страницы H1'), max_length=255, blank=True, null=True)
    title           = models.CharField(_(u'#Заголовок окна'), max_length=255, blank=True, null=True)
    keywords        = models.CharField(_(u'#Ключевые слова'), max_length=255, blank=True, null=True)
    description     = models.CharField(_(u'#Описание раздела'), max_length=255, blank=True, null=True)

    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('pages', args=[self.url])

    class Meta:
        verbose_name = _(u'#Страницы')
        verbose_name_plural = _(u'#Страницы')


class UploadFiles(models.Model):
    """ Модель загрузки файлов """

    def upload_rename(object, filename):
        # переименовываем файл на моменте сохранения в модель
        filename, format = filename.rsplit('.', 1)
        import random
        rand3 = u'%s' % (random.randint(999999, 99999999999) * random.randint(999, 9999))
        return 'upload/allfiles/' + md5(rand3)[:15] + '.' + format

    name            = models.CharField(_(u'#Название файла'), max_length=255)
    file            = models.FileField(_(u'#Файл'), upload_to=upload_rename, blank=True, null=True)
    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name


    class Meta:
        verbose_name = _(u'#Загрузка файлов')
        verbose_name_plural = _(u'#Загрузка файлов')



class News(models.Model):
    """ Модуль управление новостями """
    def upload_rename(object, filename):
        # переименовываем файл на моменте сохранения в модель
        filename, format = filename.rsplit('.', 1)
        import random
        rand3 = u'%s' % (random.randint(999999, 99999999999) * random.randint(999, 9999))
        return 'upload/news/prew_' + md5(rand3)[:15] + '.' + format

    name            = models.CharField(_(u'#Заголовок'), max_length=255)
    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данная новость"), null=True,)
    show            = models.BooleanField(_(u'#Включена'), default=True)
    min_images      = models.ImageField(_(u'#Картинка новости'), help_text=_(u"Картинка автоматом меньшится до 100х100 пикселей"), upload_to=upload_rename, null=True) #'upload/artist'
    min_text        = models.TextField(_(u'#Краткий текст новости'), null=True,)
    max_text        = HTMLField(_(u'#Подробный текст новости'), null=True,)

    #SEO
    title           = models.CharField(_(u'#Заголовок окна'), max_length=255, blank=True, null=True)
    keywords        = models.CharField(_(u'#Ключевые слова'), max_length=255, blank=True, null=True)
    description     = models.CharField(_(u'#Описание раздела'), max_length=255, blank=True, null=True)

    created         = models.DateTimeField(_(u'#Дата создания'), default=datetime.now,)

    def save(self, size=(100, 100)):
        if not self.title: # Если нет титлы, воруем из названия новости
            self.title = self.name
        if not self.keywords: # Если нет ключевиков, воруем из названия новости
            self.keywords = self.name
        if not self.description: # Если нет описания, воруем из названия новости
            self.description = self.name
        super(News, self).save()

        if self.min_images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении новости =-"
            filename = self.min_images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('news_one', args=[self.section.url, self.pk])

    class Meta:
        verbose_name = _(u'#Новости')
        verbose_name_plural = _(u'#Новости')
        ordering = ("-created",)


class Voting(models.Model):
    """ Модуль управление опросами """
    name            = models.CharField(_(u'#Название'), max_length=255)
    section         = models.ForeignKey(Section, verbose_name=_(u"Разделы"), help_text=_(u"Выбрать раздел к которому относится данный опрос"), null=True,)
    show            = models.BooleanField(_(u'#Включен'), default=True)
    report          = models.ManyToManyField("Report", verbose_name=_(u"Список ответов на вопрос"), related_name="report_voting", null=True, blank=True)

    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'#Опрос')
        verbose_name_plural = _(u'#Опросы')

class Report(models.Model):
    """ Ответы на опросы """
    name            = models.CharField(_(u'#Название'), max_length=255)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=0,)
    voting          = models.ForeignKey(Voting, verbose_name=_(u"Опрос"), help_text=_(u"Выбрать опрос к которому относится данный ответ"), related_name="voting_report", null=True)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'#Ответ на опрос')
        verbose_name_plural = _(u'#Ответ на опросы')
        ordering = ("sort",)


class ResultVoting(models.Model):
    """ Результаты опроса """
    voting          = models.ForeignKey(Voting, verbose_name=_(u"Опрос"), null=True,)
    report          = models.ForeignKey(Report, verbose_name=_(u"Ответ"), null=True,)
    ip_user         = models.IPAddressField(_(u"IP адрес проголосовавшего"), null=True)

    created         = models.DateTimeField(_(u'#Дата создания'), auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.ip_user

    class Meta:
        verbose_name = _(u'#Результат опроса')
        verbose_name_plural = _(u'#Результат опроса')
        unique_together = (('voting', 'ip_user'))