# -*- coding: utf-8 -*-

from django.contrib import admin
from tinymce.widgets import TinyMCE
from django.db import models
from cms.models import Sites, Section, Modules, Pages, UploadFiles, News, Voting, Report, ResultVoting, SubSection
from django.contrib.sites.models import Site

class SitesAdmin(admin.ModelAdmin):
    list_display = ('domain', 'name', 'root', 'show', 'sort', 'sms_name',)
    list_display_links = ('name', 'domain')
    ordering = ('sort',)
    fieldsets = (
                (u'Настройки', {'fields': ('domain', 'name', 'menu_name', 'root', 'show', 'sort',)}),
                (u'Информация', {'fields': ('about', 'meta', 'robots', 'footer', 'contacts', 'footer_contacts', 'sms_name', )}),
                (u'SEO', {'fields': ('title', 'keywords', 'description',)}),
                 )


class SectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'site', 'show', 'sort', 'created',)
    list_display_links = ('name', 'url', 'site',)
    ordering = ('sort',)
    radio_fields = {"site": admin.HORIZONTAL }
    list_editable = ("sort",)

class SubSectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'section', 'url', 'site', 'show', 'sort', 'created',)
    list_display_links = ('section', 'name', 'url', 'site',)
    ordering = ('section', 'sort',)
    list_filter = ('section', 'show',)
    radio_fields = {"site": admin.HORIZONTAL }
    list_editable = ("sort",)

class ModulesAdmin(admin.ModelAdmin):
    list_display = ('name', 'show', 'created',)
    list_display_links = ('name',)

class PagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'section', 'show', 'created',)
    list_display_links = ('name', 'url', 'section',)
    list_filter = ('section', 'show',)
    # formfield_overrides = {models.TextField: {'widget': TinyMCE()}, }

class UploadFilesAdmin(admin.ModelAdmin):
    list_display = ('name', 'file', 'created',)
    list_display_links = ('name', 'file',)
    ordering = ('-created',)


class NewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'min_images', 'section', 'show', 'created',)
    list_display_links = ('name', 'section',)
    list_filter = ('section', 'show', 'created',)
    ordering = ('-created',)
    fieldsets = (
                (u'Новость', {'fields': ('name', 'min_images', 'min_text', 'max_text', 'created',)}),
                (u'Настройки', {'fields': ('show', 'section', )}),
                (u'SEO', {'fields': ('title', 'keywords', 'description',)}),
                 )

class VotingAdmin(admin.ModelAdmin):
    list_display = ('name', 'section', 'show', 'created',)
    list_display_links = ('name', 'section',)
    list_filter = ('section', 'show', 'created',)
    ordering = ('-created',)

class ReportAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort', 'voting',)
    list_display_links = ('name', 'sort', 'voting',)


class ResultVotingAdmin(admin.ModelAdmin):
    list_display = ('voting', 'report', 'ip_user', 'created',)
    list_display_links = ('voting', 'report', 'ip_user',)
    list_filter = ('created',)
    ordering = ('-created',)

admin.site.register(Sites, SitesAdmin)
admin.site.unregister(Site) # Уберём из админки дефолтовый, чтобы не пугать всех
admin.site.register(Section, SectionAdmin)
admin.site.register(SubSection, SubSectionAdmin)
admin.site.register(Modules, ModulesAdmin)
admin.site.register(UploadFiles, UploadFilesAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Pages, PagesAdmin)
admin.site.register(Voting, VotingAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(ResultVoting, ResultVotingAdmin)

