#-*- coding:utf-8 -*-
import re
from annoying.decorators import render_to, ajax_request
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, InvalidPage, EmptyPage, PageNotAnInteger
import math

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import AnonymousUser

from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from django.contrib.sites.models import Site
from cms.models import Section, Pages, News, Voting, Report, ResultVoting, SubSection
from person.models import Person
from functions.tools import gen_page_list
from seo.tools import seo_default


@render_to('index.html')
def index(request):
    # Выводим главную
    return {'request': request}

# @render_to('cms/robots.html')
def robotstxt(request):
    # Генерируем robots.txt
    sites = Site.objects.get((Q(domain=request.META['HTTP_HOST']) | Q(sites__root=True)), Q(sites__show=True))
    return render_to_response('cms/robots.html', {'robots':sites}, mimetype="text/plain; charset=utf-8")

def js_lang(request):
    # Генерируем robots.txt
    from cms.lang import LANGUAGE
    return render_to_response('cms/js_lang.html', {'js_lang':LANGUAGE}, mimetype="text/plain; charset=utf-8")

@render_to('cms/section.html')
def section(request, url):
    """
    Вывод раздела
    """
    section = get_object_or_404(Section, url=url, show=True)
    seo_data = {
        "__name__": section.name,
            }
    seo = seo_default(seo_slug=['title_cms_section', 'h1_cms_section', 'keywords_cms_section', 'description_cms_section', 'seotext_cms_section'], obj=section, seo_data=seo_data)
    #seo = {
    #    'h1': section.h1 if section.h1 else section.name,
    #    'title': section.title,
    #    'keywords': section.keywords,
    #    'description': section.description,
    #}
    return {'request':request, 'seo':seo, 'section':section}

@render_to('cms/section.html')
def subsection(request, url=False, suburl=False):
    """
    Вывод раздела
    """
    section = get_object_or_404(SubSection, url=suburl, show=True)
    seo_data = {
        "__name__": section.name,
            }
    seo = seo_default(seo_slug=['title_cms_subsection', 'h1_cms_subsection', 'keywords_cms_subsection', 'description_cms_subsection', 'seotext_cms_subsection'], obj=section, seo_data=seo_data)
    #seo = {
    #    'h1': section.h1 if section.h1 else section.name,
    #    'title': section.title,
    #    'keywords': section.keywords,
    #    'description': section.description,
    #}
    return {'request':request, 'seo':seo, 'section':section}

@render_to('cms/pages.html')
def pages(request, pageid=None, pageurl=None):
    """
    Вывод текстовой страницы
    """
    if pageid:
        pages = get_object_or_404(Pages, pk=pageid, show=True)
    elif pageurl:
        pages = get_object_or_404(Pages, url=pageurl, show=True)
    else:
        raise Http404
    if pages.section:
        request.subsection_url = pages.section.url

    seo = seo_default(seo_slug=['title_cms_pages', 'h1_cms_pages', 'keywords_cms_pages', 'description_cms_pages', 'seotext_cms_pages'], obj=pages)
    #seo = {
    #    'h1': pages.h1 if pages.h1 else pages.name,
    #    'title': pages.title,
    #    'keywords': pages.keywords,
    #    'description': pages.description,
    #}
    #request.site_current = request.site_main
    return {'request':request, 'seo':seo, 'pages':pages,}


@render_to('cms/news.html')
def news(request, sect_path, page_id=1):
    """
    Вывод списка новостей
    """
    hosts = request.META['PATH_INFO'].split("/")
    if hosts:
        section = Section.objects.filter(url=hosts[1])
        if section:
            subsection = SubSection.objects.filter(section=section[0], url=hosts[2])
            if subsection:
                seo = seo_default(seo_slug=['title_cms_section', 'h1_cms_section', 'keywords_cms_section', 'description_cms_section', 'seotext_cms_section'], obj=subsection[0])


    news = News.objects.filter(show=True, section__url=sect_path).order_by('-created')
    item_pages = 2 # количество новостей на страницу
    page = int(page_id) # текущая страница

    paginator = Paginator(news, item_pages) # Пускай пагинатор занимается выводом новостей, циферки я сам сделаю
    page_count = news.count()
    items_rows = int(math.ceil(float(page_count) / float(item_pages))) # Сколько страниц получится
    # Небольшая функция для генерации списка циферок страниц << 1..4,5,[6],7,8..160 >>
    pages_list = gen_page_list(int(page), int(items_rows))

    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        # Если страница не является целым числом, поставить первую страницу.
        news = paginator.page(1)
    except EmptyPage:
        # Если страница находится вне диапазона (например, 9999), поставить последнюю страницу результатов.
        news = paginator.page(paginator.num_pages)

    return {'request':request, 'news':news, 'pages_list': pages_list, 'page_id': int(page_id), 'sect_path':sect_path, 'seo': seo}

@render_to('cms/news_one.html')
def news_one(request, sect_path, id):
    """
    Вывод одной новости
    """
    news = get_object_or_404(News, pk=id, show=True)
    seo = seo_default(seo_slug=['title_cms_news', 'h1_cms_news', 'keywords_cms_news', 'description_cms_news', 'seotext_cms_news'], obj=news)
    #seo = {
    #    'title': news.title,
    #    'keywords': news.keywords,
    #    'description': news.description,
    #}
    return {'request':request, 'seo':seo, 'news':news,}


@ajax_request
def voting_add(request):
    """
    AJAX форма голосования
    """
    message = u'%s' % _(u"Произошла ошибка")
    ip_address = request.META.get("REMOTE_ADDR", None)
    if request.is_ajax() and request.method == 'POST':
        data = request.POST.copy()

        reporting = data.get('voting', None)
        # Ищим ответ по которому пришёл ИД на голосование
        report = Report.objects.filter(pk=reporting)
        if report:
            report = report[0]
            #Нашли ответ, теперь ищем небыло ли голосования от этого юзверя
            result = ResultVoting.objects.filter(voting=report.voting, ip_user=ip_address)
            if not result:
                # юзер не голосовал ещё в этом опросе. Дадим ему такое право
                result = ResultVoting()
                result.report = report
                result.voting = report.voting
                result.ip_user = ip_address
                result.save()
                # А теперь уличная магия. Высчитываем по новой длинну полосочек для всех ответов
                newresult = ResultVoting.objects.filter(voting=report.voting)
                proc = 120 / newresult.count()
                arr_res = {}
                for newres in newresult:
                    if newres.report_id in arr_res:
                        arr_res[int(newres.report_id)] = arr_res[int(newres.report_id)] + 1
                    else:
                        arr_res[int(newres.report_id)] = 1

                for arr in arr_res:
                    arr_res[arr] = arr_res[arr] * proc


                return {'status':True, 'result': arr_res}
            else:
                message = u'%s' % _(u"Вы голосовали уже")
                return {'status':False, 'message': message}


    return {'status':False, 'message': message}

@render_to('sub_index.html')
def test_index(request):
    # Шаманю вёрстку главной
    return {'request': request}

