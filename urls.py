#-*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'cms.views.index', name='index'),
    url(r'^test_index/$', 'cms.views.test_index', name='test_index'),


    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^tinymce/', include('tinymce.urls')),

    url(r'^pages/(?P<pageid>\d+)/$', 'cms.views.pages', name='pages'),
    url(r'^pages/(?P<pageurl>\w+)/$', 'cms.views.pages', name='pages'),

    url(r'^(?P<sect_path>\w+)/news/$', 'cms.views.news', name='news'),
    url(r'^(?P<sect_path>\w+)/news/num_(?P<page_id>\d+)/$', 'cms.views.news', name='news_page'),
    url(r'^(?P<sect_path>\w+)/news/(?P<id>\d+)/$', 'cms.views.news_one', name='news_one'),

    url(r'^voting_add/$', 'cms.views.voting_add', name='voting_add'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT}),

    url(r'^robots.txt$', 'cms.views.robotstxt', name='robotstxt'),
    url(r'^js_lang.js$', 'cms.views.js_lang', name='js_lang'),


)

# Пользователи
urlpatterns += patterns('',
    url(r'^user/', include('person.urls')),
)

# Туры
urlpatterns += patterns('',
    url(r'^tours/', include('tours.urls')),
)

# Недвижимость
urlpatterns += patterns('',
    url(r'^nedvizhimost/', include('realty.urls')),
)

# Отели
urlpatterns += patterns('',
    url(r'^hotels/', include('hotels.urls')),
)

# SEO
urlpatterns += patterns('',
    url(r'^seo/', include('seo.urls')),
)

# Обратная связь
urlpatterns += patterns('',
    url(r'^(\w+)/feedback/', include('feedback.urls')),
)



# разделы
urlpatterns += patterns('',
    url(r'^(\w+)/(?P<suburl>\w+)/$', 'cms.views.subsection', name='sub_section'),
    url(r'^(?P<url>\w+)/$', 'cms.views.section', name='mega_section'),

)

if settings.DEBUG:
    if settings.MEDIA_ROOT:
        urlpatterns += static(settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)
    # Эта строка опциональна и будет добавлять url'ы только при DEBUG = True
    urlpatterns += staticfiles_urlpatterns()